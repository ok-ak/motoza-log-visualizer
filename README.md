## Disclaimer
This project is in no way affiliated with Motoza. I wanted a way to see the data I was collecting using Motoza's software for a car. That's all!

# Links
[motoza-log-visualizer.surge.sh](http://motoza-log-visualizer.surge.sh)

# Background
Motoza is a German car performance company located in the USA. They are known for their remote tuning capabilities. This means they do not need you to bring a car on-site for dynoing in order to generate a tune for said car.

To do this, the company offers a desktop application and a OBDII-to-USB dongle. The software and dongle are necessary for both collecting (or logging) data from the car's ECU (Electronic Control Unit) and to install custom tunes supplied by Motoza.

# So Why This?
This app plots the logging data collected by Motoza's desktop software in your browser. You go to motoza-log-visualizer.surge.sh then your upload your log file, then you sit back and watch.

By default, the data is exported into a CSV file. This data can definitely be visualized using plot functionality in Excel or similar spreadsheet software. I just wanted something that was a little more automated and portable.

# Some Final Thoughts
In theory your could log a gigantic CSV file, then upload the file to the web app and crash it but then why? It would make more sense to log a smaller file since you don't need a ton of data to get a proper tune.

Anywho, this app is for funsies and is not guarenteed in anyway.