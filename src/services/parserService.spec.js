//@flow
import { parserService } from './parserService';
import MotozaLog from '../dto/MotozaLog';
import Response from '../dto/Response';

describe('parserService', () => {
  describe('_sanitize', () => {
    it('returns after the following is completed in this order -> find /[\n]/g and replace matches with ; character then /[^a-zA-Z\d\,\:\.\-\; ]/g and replace matches with "", then lowercase everything and replace " " with "_"', () => {
      //given
      const dirtyStr: string = "ï»¿A!@#B$%^C^&*D&*(E)_-+F={}[]G;\"<>H?/~`IJKLMNOPQRSTUVWXYZ1234567890    \nabcdefghijklmnopqrstuvwxyz, . :-|";
      const expectedStr: string = "abcde-fg;hijklmnopqrstuvwxyz1234567890____;abcdefghijklmnopqrstuvwxyz,_._:-"
      
      //when
      const actualStr: string = parserService._sanitize(dirtyStr);
  
      //then
      expect(actualStr).toEqual(expectedStr);
    });
  });

  describe('_removeEmptyRows', () => {
    it('returns a new array without blank rows or rows filled with only spaces and commas', () => {
      //given
      const dirtyArr: string[] = [
        ",,,,,,,",
        ",  , ,  , ",
        ", ,,  , , , a , , ,",
        "1,2,3"
      ];
      const expectedArr: string[] = [
        ", ,,  , , , a , , ,",
        "1,2,3"
      ];
      
      //when
      const actualArr: string = parserService._removeEmptyRows(dirtyArr);
  
      //then
      expect(actualArr).toEqual(expectedArr);
    });
  });

  describe('_removeEmptyRows', () => {
    it('returns a new array without blank rows or rows filled with only spaces and commas', () => {
      //given
      const dirtyArr: string[] = [
        ",,,,,,,",
        ",  , ,  , ",
        ", ,,  , , , a , , ,",
        "1,2,3"
      ];
      const expectedArr: string[] = [
        ", ,,  , , , a , , ,",
        "1,2,3"
      ];
      
      //when
      const actualStr: string = parserService._removeEmptyRows(dirtyArr);
  
      //then
      expect(actualStr).toEqual(expectedArr);
    });
  });

  describe('_removePeriods', () => {
    it('returns a string after the following is completed in this order -> find /[^a-z\d]+/g and replaces "." with ""', () => {
      //given
      const rawStr: string= "time,eng.ine_rpm,thr...ottle....._plate,en.gine_.load,retard_cyl_1";
      const expectedStr: string = "time,engine_rpm,throttle_plate,engine_load,retard_cyl_1"
      
      //when
      const actualStr: string = parserService._removePeriods(rawStr);
  
      //then
      expect(actualStr).toEqual(expectedStr);
    });
  });

  describe('parse', () => {
    describe('empty string input', () => {
      it('returns a object with an empty data property and an errors property with "ParserService Error: Invalid string for parse"', () => {
        //given
        const inputStr: string = "";
        const expectedResponse: Response = new Response();
        expectedResponse.errors.push("ParserService Error: Invalid string for parse");

        //when
        const actualResponse = parserService.parse(inputStr);

        //then
        expect(actualResponse).toEqual(expectedResponse);
      });
    });

    describe('null string input', () => {
      it('returns a object with an empty data property and an error property with "ParserService Error: Invalid string for parse"', () => {
        //given
        const inputStr = null;
        const expectedResponse: Response = new Response();
        expectedResponse.errors.push("ParserService Error: Invalid string for parse");

        //when
        const actualResponse = parserService.parse(inputStr);

        //then
        expect(actualResponse).toEqual(expectedResponse);
      });
    });

    describe('undefined string input', () => {
      it('returns a object with an empty data property and an error property with "ParserService Error: Invalid string for parse"', () => {
        //given
        const inputStr = undefined;
        const expectedResponse: Response = new Response();
        expectedResponse.errors.push("ParserService Error: Invalid string for parse");

        //when
        const actualResponse = parserService.parse(inputStr);

        //then
        expect(actualResponse).toEqual(expectedResponse);
      });
    });

    describe('valid string input', () => {
      it('returns an object with arrays from a comma (object parameter or column) and /n (row) delimited string and empty error property', () => {
        //given
        const rawStr: string = ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n"+
          "ï»¿Time,Engine RPM,Throttle Plate,Engine Load,Ignition Angle,Mass Air Flow,Air Fuel Ratio,N75 Duty Cycle,Intake Air Temp,Boost Request,Boost Actual,Engine RPM,Engine Load,Lambda Control,AFR Desired,AFR Actual,Battery Voltage,EGT Model,Inj. On Time,Additive Trim,Multipl. Trim,Engine RPM,Vehicle Speed,Ambient Press.,Boost Request,Boost Actual,Selected Gear,Specified Load,Corrected Load,Requested Load,Intake Air Temp,Engine RPM,Engine Load,Air Fuel Ratio,Misfire Count,Ignition Angle,Intake Air Temp,Ign Retard Cyl 1,Ign Retard Cyl 2,Ign Retard Cyl 3,Ign Retard Cyl 4\n"+
          "00:00.05,2600,65.882376,102,11.25,55.0555996,0.966065937,46.875,24.75,1422.734375,1346.640625,2600,65.882376,0.999603988,0.975831577,0.966065937,14.2208,535,13.192,-0.28125,1.022309082,2600,33.9296875,1004.882813,1422.734375,1346.640625,2,164.8125,163.9921875,105.9140625,24.75,2600,65.882376,0.966065937,0,11.25,24.75,0,-1.5,0,0";
    
        const expectedObj: Response = new Response();
        expectedObj.data.time.push('00:00.05');
        expectedObj.data.engine_rpm.push(2600);
        expectedObj.data.throttle_plate.push(65.882376);
        expectedObj.data.engine_load.push(102);
        expectedObj.data.ignition_angle.push(11.25);
        expectedObj.data.mass_air_flow.push(55.0555996);
        expectedObj.data.air_fuel_ratio.push(0.966065937);
        expectedObj.data.n75_duty_cycle.push(46.875);
        expectedObj.data.intake_air_temp.push(24.75);
        expectedObj.data.boost_request.push(1422.734375);
        expectedObj.data.boost_actual.push(1346.640625);
        expectedObj.data.engine_rpm_2.push(2600);
        expectedObj.data.engine_load_2.push(65.882376);
        expectedObj.data.lambda_control.push(0.999603988);
        expectedObj.data.afr_desired.push(0.975831577);
        expectedObj.data.afr_actual.push(0.966065937);
        expectedObj.data.battery_voltage.push(14.2208);
        expectedObj.data.egt_model.push(535);
        expectedObj.data.inj_on_time.push(13.192);
        expectedObj.data.additive_trim.push(-0.28125);
        expectedObj.data.multipl_trim.push(1.022309082);
        expectedObj.data.engine_rpm_3.push(2600);
        expectedObj.data.vehicle_speed.push(33.9296875);
        expectedObj.data.ambient_press.push(1004.882813);
        expectedObj.data.boost_request_2.push(1422.734375);
        expectedObj.data.boost_actual_2.push(1346.640625);
        expectedObj.data.selected_gear.push(2);
        expectedObj.data.specified_load.push(164.8125);
        expectedObj.data.corrected_load.push(163.9921875);
        expectedObj.data.requested_load.push(105.9140625);
        expectedObj.data.intake_air_temp_2.push(24.75);
        expectedObj.data.engine_rpm_4.push(2600);
        expectedObj.data.engine_load_3.push(65.882376);
        expectedObj.data.air_fuel_ratio_2.push(0.966065937);
        expectedObj.data.misfire_count.push(0);
        expectedObj.data.ignition_angle_2.push(11.25);
        expectedObj.data.intake_air_temp_3.push(24.75);
        expectedObj.data.ign_retard_cyl_1.push(0);
        expectedObj.data.ign_retard_cyl_2.push(-1.5);
        expectedObj.data.ign_retard_cyl_3.push(0);
        expectedObj.data.ign_retard_cyl_4.push(0);
        
        //when
        const actualObj: Response = parserService.parse(rawStr);

        //then
        expect(actualObj).toEqual(expectedObj);
      });
    });
  });
});