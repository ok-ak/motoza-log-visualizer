import React from 'react';
import './BottomBanner.css'

const styles = {
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
}

const BottomBanner = () => {

  return (
    <div className="BottomBanner-container" style={styles.container}>
      <h3>Questions, Comments, Feedback? Checkout this project on GitLab</h3>
      <a 
        href="https://gitlab.com/ok-ak/motoza-log-visualizer"
        className="BottomBanner-link">
          Click Here
        </a>
    </div>
  )
}

export default BottomBanner;