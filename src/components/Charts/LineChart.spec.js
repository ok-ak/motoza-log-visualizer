//@flow
import React from 'react';
import ReactDOM from 'react-dom';
import { mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { until, createShallow, createMount } from '@material-ui/core/test-utils';
import LineChart from '../Charts/LineChart';

describe('LineChart', () => {
  describe('chartMidIndex state variable', () => {
    it.skip('defaults to max data point in the array belonging to this.chartData.mass_air_flow', () => {
      //given
      const mockData = {
        mass_air_flow: [ 1, 2, 3, 4, 5, 6, 7 ],
        time: [ "00:00", "00:01"],
        dragonball: ["that cat with the sensu beans", "beerus", "SSG Goku blue"]
      }
  
      const expectedChartMidIndex = 3;
  
      //when
      const shallow = createShallow()
      const component = shallow(
        <LineChart data={mockData} />
      );
  
  
      console.log('!!!!!!!!!!!!'), component.debug({verbose: true})
  
      const actualChartMidIndex = component.find(LineChart).state('chartMidIndex');
  
      //then
      expect(actualChartMidIndex).toEqual(expectedChartMidIndex);
    });
  })
});