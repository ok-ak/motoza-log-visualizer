//@flow
import React from 'react';
import ReactDOM from 'react-dom';
import { mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { until, createShallow, createMount } from '@material-ui/core/test-utils';
import FulLScreenDialog from './FullScreenDialog';
import Input from '@material-ui/core/Input';
import CircularProgess from '@material-ui/core/CircularProgress';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import FullScreenDialog, { VisualizeButton } from './FullScreenDialog';
import LineChart from '../Charts/LineChart';

describe('FulLScreenDialog', () => {
  describe('Visualize Button', () => {
    it('was created', () => {
      //when
      const shallow = createShallow()
      const component = shallow(
        <VisualizeButton buttonText='Visualize' />
      );

      const buttonComponent = component.find('#FullScreebDialog-button-visualize');
      const buttonText = buttonComponent.text();

      //then
      expect(buttonText).toEqual('Visualize');
    });

    it('has disabled visualize button when props.hasData is false', () => {
      //when
      const shallow = createShallow()
      const component = shallow(
        <VisualizeButton buttonText='Visualize' hasData={false}/>
      );

      const buttonComponent = component.find('#FullScreebDialog-button-visualize');
      const buttonProp = buttonComponent.prop('disabled');

      //then
      expect(buttonProp).toEqual(true);
    });

    it('has enabled visualize button when props.hasData is true', () => {
      //when
      const shallow = createShallow()
      const component = shallow(
        <VisualizeButton buttonText='Visualize' hasData={true}/>
      );

      const buttonComponent = component.find('#FullScreebDialog-button-visualize');
      const buttonProp = buttonComponent.prop('disabled');

      //then
      expect(buttonProp).toBeFalsy();
    });
  });

  describe('CircularProgess Loader', () => {
    it('exists when props.data === undefined', () => {
      //when
      const shallow = createShallow()
      const component = shallow(
        <FullScreenDialog />
      );

      const progressComponent = component.find('CircularProgress');

      //then
      expect(progressComponent).toBeTruthy();
    });

    it('does not get rendered when props.data is truthy', () => {
      //when
      const shallow = createShallow()
      const component = shallow(
        <FullScreenDialog data={'jedi temple archives and all of starfleet databases'}/>
      );

      const progressComponent = component.find('CircularProgress');

      //then
      expect(progressComponent).toEqual({});
    });
  });
  describe.skip('FormControl', () => {
    it('does not exist when props.data === undefined', () => {
      //when
      const shallow = createShallow()
      const component = shallow(
        <FullScreenDialog />
      );

      const formControlComponent = component.find(FormControl);

      //then
      expect(formControlComponent).to.have.lengthOf(0);
    });

    it('exists when props.data is truthy', () => {
      //when
      const shallow = createShallow()
      const component = shallow(
        <FullScreenDialog data={'Goku was here'}/>
      );

      const formControlComponent = component.find(FormControl);

      console.log('!!!!!!!', formControlComponent.debug({verbose: true}));

      //then
      expect(formControlComponent).to.have.lengthOf(1);;
    });
  });
});