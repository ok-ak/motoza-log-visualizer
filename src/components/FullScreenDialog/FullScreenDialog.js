//@flow
// YUP THIS IS A STRAIGHT COPY/PASTE FROM MATERIAL-UI DIALOG DEMOS PAGE WITH MINOR TWEAKS
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import CircularProgress from '@material-ui/core/CircularProgress';
import LineChart from '../Charts/LineChart';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import ArrowBackIos from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIos from '@material-ui/icons/ArrowForwardIos';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  circularProgressContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%'
  },
  chartSelect: {
    margin: '16px 8px',
    minWidth: '200px'
  }
}));

type Props = {
  data: any,
  hasData: boolean,
  buttonText: string,
  handleClickOpen: void,
  submitHandler: void
}

export function VisualizeButton(props: Props) {
  if(props.hasData){
    return (
      <Button id="FullScreebDialog-button-visualize" variant="outlined" color="primary" onClick={props.handleClickOpen}>
        {props.buttonText}
      </Button>
    )
  }
  return (
    <Button id="FullScreebDialog-button-visualize" variant="outlined" color="primary" disabled onClick={props.handleClickOpen}>
      {props.buttonText}
    </Button>
  )
}

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog(props: Props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [chartData, setChartData] = React.useState(undefined);
  const [chartSelect, setChartSelect] = React.useState("mass_air_flow");

  useEffect(() => {
    setChartData( props.data);
    // setChartMidIndex(computeChartMidIndex())
  });

  function handleClickOpen(): void {
    props.submitHandler();
    setOpen(true);
  }

  function handleClose(): void {
    setOpen(false);
  }

  function willShowLineChart() {
    if(!chartData || chartData === null) {
      return (
        <div className={classes.circularProgressContainer}>
          <CircularProgress
            color="secondary"
            size={50}
            variant="indeterminate"
            disableShrink 
            thickness={3} />
        </div>
      )
    }
    return (
      <div>
        <FormControl variant="outlined" className = {classes.chartSelect}>
          <InputLabel htmlFor="outlined-age-simple">
            Pick a dataset
          </InputLabel>
          <Select
            value={chartSelect}
            onChange={handleChange}>
            {Object.keys(props.data).map((el, index) => {
              return (
                <MenuItem value={el} key={index}>{el}</MenuItem>
              )
            })}
          </Select>
        </FormControl>
        {/* <div className={classes.chartContainer}>
          <div className={classes.chartButton}>
            <ArrowBackIos/>
          </div>
          <div className = {classes.chartMainContent}> */}
            <LineChart
              dataSelection = {chartSelect}
              data = {chartData} />
          {/* </div>
          <div className={classes.chartButton}>
            <ArrowForwardIos/>
          </div>
        </div> */}
      </div>
    )
  }

  const handleChange = (event: Event) => {
    console.log("@@@", event.target.value);
    setChartSelect(event.target.value);
  }

  return (
    <div>
      <VisualizeButton hasData={props.hasData} buttonText={props.buttonText} handleClickOpen={handleClickOpen}/>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              The Best Chart ᕙ(⇀‸↼‶)ᕗ
            </Typography>
          </Toolbar>
        </AppBar>
        {/* TODO ADD CHARTJS LINE CHART */}
        {willShowLineChart()}
      </Dialog>
    </div>
  );
}
