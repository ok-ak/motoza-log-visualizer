//@flow

export default class MotozaLog {
  time: string[]
  engine_rpm: number[]
  throttle_plate: number[]
  engine_load: number[]
  ignition_angle: number[]
  mass_air_flow: number[]
  air_fuel_ratio: number[]
  n75_duty_cycle: number[]
  intake_air_temp: number[]
  boost_request: number[]
  boost_actual: number[]
  engine_rpm_2: number[]
  engine_load_2: number[]
  lambda_control: number[]
  afr_desired: number[]
  afr_actual: number[]
  battery_voltage: number[]
  egt_model: number[]
  inj_on_time: number[]
  additive_trim: number[]
  multipl_trim: number[]
  engine_rpm_3: number[]
  vehicle_speed: number[]
  ambient_press: number[]
  boost_request_2: number[]
  boost_actual_2: number[]
  selected_gear: number[]
  specified_load: number[]
  corrected_load: number[]
  requested_load: number[]
  intake_air_temp_2: number[]
  engine_rpm_4: number[]
  engine_load_3: number[]
  air_fuel_ratio_2: number[]
  misfire_count: number[]
  ignition_angle_2: number[]
  intake_air_temp_3: number[]
  ign_retard_cyl_1: number[]
  ign_retard_cyl_2: number[]
  ign_retard_cyl_3: number[]
  ign_retard_cyl_4: number[]

  constructor() {
    this.time = [];
    this.engine_rpm = [];
    this.throttle_plate = [];
    this.engine_load = [];
    this.ignition_angle = [];
    this.mass_air_flow = [];
    this.air_fuel_ratio = [];
    this.n75_duty_cycle = [];
    this.intake_air_temp = [];
    this.boost_request = [];
    this.boost_actual = [];
    this.engine_rpm_2 = [];
    this.engine_load_2 = [];
    this.lambda_control = [];
    this.afr_desired = [];
    this.afr_actual = [];
    this.battery_voltage = [];
    this.egt_model = [];
    this.inj_on_time = [];
    this.additive_trim = [];
    this.multipl_trim = [];
    this.engine_rpm_3 = [];
    this.vehicle_speed = [];
    this.ambient_press = [];
    this.boost_request_2 = [];
    this.boost_actual_2 = [];
    this.selected_gear = [];
    this.specified_load = [];
    this.corrected_load = [];
    this.requested_load = [];
    this.intake_air_temp_2 = [];
    this.engine_rpm_4 = [];
    this.engine_load_3 = [];
    this.air_fuel_ratio_2 = [];
    this.misfire_count = [];
    this.ignition_angle_2 = [];
    this.intake_air_temp_3 = [];
    this.ign_retard_cyl_1 = [];
    this.ign_retard_cyl_2 = [];
    this.ign_retard_cyl_3 = [];
    this.ign_retard_cyl_4 = [];
  }
}